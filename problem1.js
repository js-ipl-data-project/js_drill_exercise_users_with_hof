
//Q1 Find all users who are interested in playing video games.


function findUsersByInterest(usersData, interest) {
    try {
    const usersWithInterest = [];

    Object.keys(usersData).forEach(userName => {
        const interests = usersData[userName].interests;
        if (interests && interests.some(int => int.includes(interest))) {
            usersWithInterest.push(userName);
        }
    });

    return usersWithInterest;
}catch (error) {
    console.error('something Went Wrong', error.message);
    return []; 
}
}

module.exports=findUsersByInterest