//Q3 Group users based on their Programming language mentioned in their designation.

const users = require("./1-users.cjs");

function groupByProgramming(usersData) {
    if(typeof usersData=="object"){
    const languages = ['Python', 'javascript', 'Golang'];

    let usersByLanguage = languages.reduce((accByLanguage, language) => {
        return Object.keys(usersData).reduce((acc, userName) => {
            if (!acc[language]) {
                acc[language] = [];
            }
            if (usersData[userName].desgination.toLowerCase().includes(language.toLowerCase())) {
                acc[language].push(userName);
            }
            return acc;
        }, accByLanguage);
    }, {});

    return usersByLanguage;
}else{
    console.log("something went wrong")
}
}
module.exports=groupByProgramming
