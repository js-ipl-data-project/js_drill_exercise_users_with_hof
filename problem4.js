
//Q4 Find all users with masters Degree.

const users = require("./1-users.cjs");

function findUsersWithMasters(usersData) {
    if(typeof usersData=="object"){
    return Object.keys(usersData).filter(userName => {
        return usersData[userName].qualification.toLowerCase().includes("masters");
    });
}else{
    console.log("some thing went wrong")
}
}

module.exports=findUsersWithMasters
